package com.ppe.dao;

import com.ppe.beans.Lieu;
import com.ppe.beans.Salle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;

public class SalleDaoImpl implements SalleDao {

    private DAOFactory          daoFactory;

    public SalleDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    
	@Override
	public void create(Salle salle) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
		Statement statement = null;
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			statement.executeUpdate("INSERT INTO salle (numsalle, numlieu, longueursalle, largeursalle, hauteursalle, surfacesalle, tarifdemijsalle) VALUES (DEFAULT, '"+salle.getLieu().getNumlieu()+"', "+salle.getLongueursalle()+", "+salle.getLargeursalle()+", "+salle.getHauteursalle()+", "+salle.getSurfacesalle()+", "+salle.getTarifdemijsalle()+")");
		} catch ( SQLException e ) {
		    throw new DAOException( e );
		}
	}

	@Override
	public ArrayList<Salle> readAll() throws DAOException {
        Connection connexion = null;
        Statement statement = null;
        Statement statement2 = null;
        ResultSet resultSet = null;
        ResultSet lieuSet = null;
        Salle salle = null;
        ArrayList<Salle> salles = new ArrayList<>();
        Lieu lieu = new Lieu();
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			statement2 = connexion.createStatement();
			
			resultSet = statement.executeQuery("SELECT * FROM salle");
			
			while ( resultSet.next() ) {
				salle = mapSalle( resultSet );
				
				lieuSet = statement2.executeQuery("SELECT lieu.numlieu,"
													+ " numville,"
													+ " numcateglieu,"
													+ " numentreprise,"
													+ " nomlieu,"
													+ " adresselieu,"
													+ " longitudelieu,"
													+ " lattitudelieu,"
													+ " nombreetoiles,"
													+ " descriptionlieu,"
													+ " annulationgratuiteo_n FROM lieu, salle WHERE salle.numlieu = lieu.numlieu and salle.numlieu ="+ resultSet.getInt("numlieu"));
				
				while (lieuSet.next() ) {
					lieu = mapLieu( lieuSet );
				}
				
				salle.setLieu(lieu);
				salles.add(salle);
			}
		} catch ( SQLException e ) {
		    throw new DAOException( e );
		}
		
		return salles;
	}

	@Override
	public void update(int numsalle, int numlieu, double longueursalle, double largeursalle, double hauteursalle,
			double surfacesalle, double tarifdemijsalle) throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int numsalle) throws DAOException {
        Connection connexion = null;
        Statement statement = null;
        try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			statement.executeUpdate("DELETE FROM salle WHERE numsalle="+ numsalle);
		} catch ( SQLException e ) {
		    throw new DAOException( e );
		}
			
		
	}

	@Override
	public Salle find(int numsalle) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}
	
    private static Salle mapSalle( ResultSet resultSet ) throws SQLException {
        Salle salle = new Salle();
        salle.setNumsalle( resultSet.getInt("numsalle") );
        salle.setLongueursalle(resultSet.getDouble("longueursalle"));
        salle.setLargeursalle(resultSet.getDouble("largeursalle"));
        salle.setHauteursalle(resultSet.getDouble("hauteursalle"));
        salle.setSurfacesalle(resultSet.getDouble("surfacesalle"));
        salle.setTarifdemijsalle(resultSet.getDouble("tarifdemijsalle"));
        return salle;
    }
    
    private static Lieu mapLieu( ResultSet resultSet) throws SQLException {
    	Lieu lieu = new Lieu();
    	lieu.setNumlieu( resultSet.getInt("numlieu") );
    	lieu.setNumville( resultSet.getInt("numville") );
    	lieu.setNumcateglieu( resultSet.getInt("numcateglieu") );
    	lieu.setNumentreprise( resultSet.getInt("numentreprise") );
    	lieu.setNomlieu( resultSet.getString("nomlieu") );
    	lieu.setAdresselieu( resultSet.getString("adresselieu") );
    	lieu.setLongitudelieu( resultSet.getDouble("longitudelieu") );
    	lieu.setLattitudelieu( resultSet.getDouble("lattitudelieu") );
    	lieu.setNombreetoiles( resultSet.getInt("nombreetoiles") );
    	lieu.setDescriptionlieu( resultSet.getString("descriptionlieu") );
    	lieu.setAnnulationgratuite( resultSet.getBoolean("annulationgratuiteo_n") );
    	return lieu;
    }


	
}
