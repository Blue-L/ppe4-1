package com.ppe.dao;

import com.ppe.beans.Utilisateur;

public interface UtilisateurDao {
	
	void create(Utilisateur utilisateur) throws DAOException;
	
	Utilisateur[] readAll() throws DAOException;
	
	Utilisateur read(int id) throws DAOException;
	
	void update( int id, String email, String password ) throws DAOException;
	
	void delete( int id ) throws DAOException;
	
	Utilisateur find( String email ) throws DAOException;
	
	Utilisateur verification ( String mail, String mdp ) throws DAOException;
}
