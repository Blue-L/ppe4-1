package com.ppe.dao;

import com.ppe.beans.Utilisateur;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;

public class UtilisateurDaoImpl implements UtilisateurDao {


    private DAOFactory          daoFactory;

    public UtilisateurDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    
	@Override
	public void create(Utilisateur utilisateur) throws IllegalArgumentException, DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
		Statement statement = null;
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			statement.executeUpdate("INSERT INTO utilisateur (idutilisateur, mailutilisateur, mdputilisateur) VALUES (DEFAULT, '"+utilisateur.getMailutilisateur()+"', '"+utilisateur.getMdputilisateur()+"')");
		} catch ( SQLException e ) {
		    throw new DAOException( e );
		}
	}

	@Override
	public Utilisateur[] readAll() throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Utilisateur read(int id) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(int id, String email, String password) throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) throws DAOException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Utilisateur find( String email ) throws DAOException {
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Utilisateur utilisateur = null;
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM utilisateur WHERE mailutilisateur='" + email + "'");
			
			if ( resultSet.next() ) {
				utilisateur = map( resultSet );
			}
		} catch ( SQLException e ) {
		    throw new DAOException( e );
		}
		
		return utilisateur;
	}
	
	@Override
	public Utilisateur verification( String mail, String mdp ) {
		// TODO Auto-generated method stub
		Connection connexion = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Utilisateur utilisateur = null;
        try {
        	connexion = daoFactory.getConnection();
        	statement = connexion.createStatement();
        	resultSet = statement.executeQuery("SELECT * FROM utilisateur WHERE mailutilisateur='" + mail + "' and mdputilisateur='" + mdp + "'");
        	
        	while ( resultSet.next() ) {
        		utilisateur = map(resultSet);
        	}
        } catch (SQLException e) {
        	throw new DAOException( e );
        }
		return utilisateur;
	}
	
    /*
     * Simple m�thode utilitaire permettant de faire la correspondance (le
     * mapping) entre une ligne issue de la table des utilisateurs (un
     * ResultSet) et un bean Utilisateur.
     */
    private static Utilisateur map( ResultSet resultSet ) throws SQLException {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdutilisateur( resultSet.getInt( "idutilisateur" ) );
        utilisateur.setMailutilisateur( resultSet.getString( "mailutilisateur" ) );
        utilisateur.setMdputilisateur( resultSet.getString( "mdputilisateur" ) );
        return utilisateur;
    }



	
}
