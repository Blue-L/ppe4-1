package com.ppe.dao;

import java.util.ArrayList;

import com.ppe.beans.Salle;

public interface SalleDao {
	
	void create(Salle salle) throws DAOException;
	
	ArrayList<Salle> readAll() throws DAOException;
	
	void update( int numsalle, int numlieu, double longueursalle, double largeursalle, double hauteursalle, double surfacesalle, double tarifdemijsalle ) throws DAOException;
	
	void delete( int numsalle ) throws DAOException;
	
	Salle find( int numsalle ) throws DAOException;
}
