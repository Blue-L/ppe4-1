package com.ppe.dao;

import java.util.ArrayList;

import com.ppe.beans.Lieu;

public interface LieuDao {
	
	ArrayList<Lieu> readAll() throws DAOException;
}
