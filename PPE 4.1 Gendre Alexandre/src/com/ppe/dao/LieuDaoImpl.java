package com.ppe.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.ppe.beans.Lieu;

public class LieuDaoImpl implements LieuDao {

    private DAOFactory          daoFactory;

    public LieuDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    
	@Override
	public ArrayList<Lieu> readAll() throws DAOException {
	  	Connection connexion = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<Lieu> lieus = new ArrayList<>();
        Lieu lieu = new Lieu();
		try {
			connexion = daoFactory.getConnection();
			statement = connexion.createStatement();
			
			resultSet = statement.executeQuery("SELECT * FROM lieu");
			
			while ( resultSet.next() ) {
				lieu = mapLieu( resultSet );
				lieus.add(lieu);
			}
		} catch ( SQLException e ) {
		    throw new DAOException( e );
		}
		
		return lieus;
	}

    private static Lieu mapLieu( ResultSet resultSet) throws SQLException {
    	Lieu lieu = new Lieu();
    	lieu.setNumlieu( resultSet.getInt("numlieu") );
    	lieu.setNumville( resultSet.getInt("numville") );
    	lieu.setNumcateglieu( resultSet.getInt("numcateglieu") );
    	lieu.setNumentreprise( resultSet.getInt("numentreprise") );
    	lieu.setNomlieu( resultSet.getString("nomlieu") );
    	lieu.setAdresselieu( resultSet.getString("adresselieu") );
    	lieu.setLongitudelieu( resultSet.getDouble("longitudelieu") );
    	lieu.setLattitudelieu( resultSet.getDouble("lattitudelieu") );
    	lieu.setNombreetoiles( resultSet.getInt("nombreetoiles") );
    	lieu.setDescriptionlieu( resultSet.getString("descriptionlieu") );
    	lieu.setAnnulationgratuite( resultSet.getBoolean("annulationgratuiteo_n") );
    	return lieu;
    }
}
