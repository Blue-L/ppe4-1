package com.ppe.beans;

public class Utilisateur {

	private int      idutilisateur;
	private String    mailutilisateur;
    private String    mdputilisateur;
	public int getIdutilisateur() {
		return idutilisateur;
	}
	public void setIdutilisateur(int idutilisateur) {
		this.idutilisateur = idutilisateur;
	}
	public String getMailutilisateur() {
		return mailutilisateur;
	}
	public void setMailutilisateur(String mailutilisateur) {
		this.mailutilisateur = mailutilisateur;
	}
	public String getMdputilisateur() {
		return mdputilisateur;
	}
	public void setMdputilisateur(String mdputilisateur) {
		this.mdputilisateur = mdputilisateur;
	}
  
}
