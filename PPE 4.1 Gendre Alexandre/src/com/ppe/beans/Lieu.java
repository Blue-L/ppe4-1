package com.ppe.beans;

public class Lieu {
	
	private int numlieu;
	private int numville;
	private int numcateglieu;
	private int numentreprise;
	private String nomlieu;
	private String adresselieu;
	private double longitudelieu;
	private double lattitudelieu;
	private int nombreetoiles;
	private String descriptionlieu;
	private boolean annulationgratuite;
	
	public int getNumlieu() {
		return numlieu;
	}
	public void setNumlieu(int numlieu) {
		this.numlieu = numlieu;
	}
	public int getNumville() {
		return numville;
	}
	public void setNumville(int numville) {
		this.numville = numville;
	}
	public int getNumcateglieu() {
		return numcateglieu;
	}
	public void setNumcateglieu(int numcateglieu) {
		this.numcateglieu = numcateglieu;
	}
	public int getNumentreprise() {
		return numentreprise;
	}
	public void setNumentreprise(int numentreprise) {
		this.numentreprise = numentreprise;
	}
	public String getNomlieu() {
		return nomlieu;
	}
	public void setNomlieu(String nomlieu) {
		this.nomlieu = nomlieu;
	}
	public String getAdresselieu() {
		return adresselieu;
	}
	public void setAdresselieu(String adresselieu) {
		this.adresselieu = adresselieu;
	}
	public double getLongitudelieu() {
		return longitudelieu;
	}
	public void setLongitudelieu(double longitudelieu) {
		this.longitudelieu = longitudelieu;
	}
	public double getLattitudelieu() {
		return lattitudelieu;
	}
	public void setLattitudelieu(double lattitudelieu) {
		this.lattitudelieu = lattitudelieu;
	}
	public int getNombreetoiles() {
		return nombreetoiles;
	}
	public void setNombreetoiles(int nombreetoiles) {
		this.nombreetoiles = nombreetoiles;
	}
	public String getDescriptionlieu() {
		return descriptionlieu;
	}
	public void setDescriptionlieu(String descriptionlieu) {
		this.descriptionlieu = descriptionlieu;
	}
	public boolean isAnnulationgratuite() {
		return annulationgratuite;
	}
	public void setAnnulationgratuite(boolean annulationgratuite) {
		this.annulationgratuite = annulationgratuite;
	}
	
	
}
