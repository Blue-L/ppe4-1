package com.ppe.beans;

import java.util.ArrayList;

public class Salle {
	
	private int numsalle;
	private double longueursalle;
	private double largeursalle;
	private double hauteursalle;
	private double surfacesalle;
	private double tarifdemijsalle;
	
	private Lieu lieu;
	
	
	public Lieu getLieu() {
		return lieu;
	}
	public void setLieu(Lieu lieu) {
		this.lieu = lieu;
	}
	public double getTarifdemijsalle() {
		return tarifdemijsalle;
	}
	public void setTarifdemijsalle(double tarifdemijsalle) {
		this.tarifdemijsalle = tarifdemijsalle;
	}
	public int getNumsalle() {
		return numsalle;
	}
	public void setNumsalle(int numsalle) {
		this.numsalle = numsalle;
	}
	public double getLongueursalle() {
		return longueursalle;
	}
	public void setLongueursalle(double longueursalle) {
		this.longueursalle = longueursalle;
	}
	public double getLargeursalle() {
		return largeursalle;
	}
	public void setLargeursalle(double largeursalle) {
		this.largeursalle = largeursalle;
	}
	public double getHauteursalle() {
		return hauteursalle;
	}
	public void setHauteursalle(double hauteursalle) {
		this.hauteursalle = hauteursalle;
	}
	public double getSurfacesalle() {
		return surfacesalle;
	}
	public void setSurfacesalle(double surfacesalle) {
		this.surfacesalle = surfacesalle;
	}
}
