package com.ppe.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ppe.beans.Lieu;
import com.ppe.beans.Salle;
import com.ppe.dao.DAOException;
import com.ppe.dao.SalleDao;

public final class AjoutSalleForm {
    
	private SalleDao      salleDao;
	
    private String              resultat;
    private Map<String, String> erreurs          = new HashMap<String, String>();
    
    private static final String CHAMP_LONGUEUR     = "longueur";
    private static final String CHAMP_LARGEUR      = "largeur";
    private static final String CHAMP_HAUTEUR      = "hauteur";
    private static final String CHAMP_SURFACE      = "surface";
    private static final String CHAMP_TARIF        = "tarif";
    private static final String CHAMP_LIEU         = "lieu";
	
	public AjoutSalleForm( SalleDao salleDao ) {
	    this.salleDao = salleDao;
	}
	
    public Salle ajoutSalle( HttpServletRequest request) {
    	
        String longueur = getValeurChamp(request, CHAMP_LONGUEUR);
        String largeur = getValeurChamp(request, CHAMP_LARGEUR);
        String hauteur = getValeurChamp(request, CHAMP_HAUTEUR);
        String surface = getValeurChamp(request, CHAMP_SURFACE);
        String tarif = getValeurChamp(request, CHAMP_TARIF);
        String lieu = getValeurChamp(request, CHAMP_LIEU);
        
        Salle salle = new Salle();
        Lieu unLieu = new Lieu();
        unLieu.setNumlieu(Integer.valueOf(lieu));
        try
        {
        	salle.setLongueursalle(Double.valueOf(longueur));
        	salle.setLargeursalle(Double.valueOf(largeur));
        	salle.setHauteursalle(Double.valueOf(hauteur));
        	salle.setSurfacesalle(Double.valueOf(surface));
        	salle.setTarifdemijsalle(Double.valueOf(tarif));
        	salle.setLieu(unLieu);
        	
            if ( erreurs.isEmpty() ) {
            	salleDao.create(salle);
                resultat = "Succ�s de l'ajout.";
            } else {
                resultat = "�chec de l'ajout.";
            }
        	
            
        } catch ( DAOException e ) {
            resultat = "�chec de l'ajout d'une salle : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        }
        
		return salle;
    	
    }
    
    public Map<String, String> getErreurs() {
        return erreurs;
    }
    
    public String getResultat() {
        return resultat;
    }
    
    /*
     * Ajoute un message correspondant au champ sp�cifi� � la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }
    
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur.trim();
        }
    }

}

