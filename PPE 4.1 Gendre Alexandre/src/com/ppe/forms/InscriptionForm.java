package com.ppe.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ppe.beans.Utilisateur;
import com.ppe.dao.DAOException;
import com.ppe.dao.UtilisateurDao;

public final class InscriptionForm {
    
	private UtilisateurDao      utilisateurDao;
	
    private String              resultat;
    private Map<String, String> erreurs          = new HashMap<String, String>();
    
    private static final String CHAMP_EMAIL      = "mail";
    private static final String CHAMP_PASS       = "mdp";
	
	public InscriptionForm( UtilisateurDao utilisateurDao ) {
	    this.utilisateurDao = utilisateurDao;
	}
	
    public Utilisateur inscrireUtilisateur( HttpServletRequest request) {
    	
        String mail = getValeurChamp(request, CHAMP_EMAIL);
        String mdp = getValeurChamp(request, CHAMP_PASS);
        
        Utilisateur utilisateur = new Utilisateur();
        try
        {
        	traiterEmail( mail, utilisateur );
        	utilisateur.setMdputilisateur(mdp);
        	
            if ( erreurs.isEmpty() ) {
                utilisateurDao.create( utilisateur );
                resultat = "Succ�s de l'inscription.";
            } else {
                resultat = "�chec de l'inscription.";
            }
            
        } catch ( DAOException e ) {
            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        }
        
		return utilisateur;
    	
    }
    
    public Map<String, String> getErreurs() {
        return erreurs;
    }
    
    public String getResultat() {
        return resultat;
    }
    
    /*
     * Appel � la validation de l'adresse email re�ue et initialisation de la
     * propri�t� email du bean
     */
    private void traiterEmail( String mail, Utilisateur utilisateur ) {
        try {
            validationEmail( mail );
        } catch ( FormValidationException e ) {
            setErreur( CHAMP_EMAIL, e.getMessage() );
        }
        utilisateur.setMailutilisateur( mail );
    }
    
    /* Validation de l'adresse email */
    private void validationEmail( String mail ) throws FormValidationException {
        if ( mail != null ) {
            if ( !mail.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new FormValidationException( "Merci de saisir une adresse mail valide." );
            }
            if (this.utilisateurDao.find( mail ) != null) {
            	throw new FormValidationException("Cette adresse mail existe d�j�.");
            }
        } else {
            throw new FormValidationException( "Merci de saisir une adresse mail." );
        }
    }
    
    /*
     * Ajoute un message correspondant au champ sp�cifi� � la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }
    
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur.trim();
        }
    }

}

