package com.ppe.forms;

public class FormValidationException extends Exception {

    public FormValidationException( String message ) {
        super( message );
    }
}