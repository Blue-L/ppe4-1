package com.ppe.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ppe.dao.SalleDao;
import com.ppe.forms.AjoutSalleForm;
import com.ppe.forms.ModifierSalleForm;
import com.ppe.beans.Salle;
import com.ppe.dao.DAOException;
import com.ppe.dao.DAOFactory;
import com.ppe.dao.LieuDao;

public class ModifierSalle extends HttpServlet {
    public static final String VUE = "/modifierSalle.jsp";
    public static final String ATT_LISTLIEUS = "lieus";
    public static final String ATT_FORM = "form";
    public static final String ATT_SALLE = "salle";
    public static final String CONF_DAO_FACTORY = "daofactory";

    private SalleDao          salleDao;
    private LieuDao			  lieuDao;

    public void init() throws ServletException {
        /* R�cup�ration d'une instance de notre DAO Utilisateur */
        this.salleDao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getSalleDao();
        this.lieuDao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getLieuDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
    	
    	ArrayList<com.ppe.beans.Lieu> lieus = this.lieuDao.readAll();
    	request.setAttribute( ATT_LISTLIEUS, lieus );
    	
    	com.ppe.beans.Salle salle = new Salle();
    	salle.setNumsalle(Integer.parseInt(request.getParameter("numsalle")));
    	salle.getLieu().setNumlieu(Integer.parseInt(request.getParameter("numlieu")));
    	
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        /* Pr�paration de l'objet formulaire */
    	ModifierSalleForm form = new ModifierSalleForm( salleDao );

        /* Appel au traitement et � la validation de la requ�te, et r�cup�ration du bean en r�sultant */
        Salle salle = form.modifierSalle( request );
        
        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute( ATT_FORM, form );
        request.setAttribute( ATT_SALLE, salle );
        
    	ArrayList<com.ppe.beans.Lieu> lieus = this.lieuDao.readAll();
    	request.setAttribute( ATT_LISTLIEUS, lieus );
        
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

}
