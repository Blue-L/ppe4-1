package com.ppe.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ppe.dao.SalleDao;
import com.ppe.dao.DAOException;
import com.ppe.dao.DAOFactory;

public class Salle extends HttpServlet {
    public static final String VUE = "/salle.jsp";
    public static final String ATT_LISTSALLE = "salles";
    public static final String CONF_DAO_FACTORY = "daofactory";

    private SalleDao          salleDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.salleDao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getSalleDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        
    	ArrayList<com.ppe.beans.Salle> salles = this.salleDao.readAll();
    	request.setAttribute( ATT_LISTSALLE, salles );
    	
    	String action = request.getParameter("action");
    	
    	if(action != null && action.equals("delete"))
    	{
    		int numsalle = Integer.parseInt(request.getParameter("numsalle"));
    		
    		this.salleDao.delete(numsalle);
    	}
    	
    	if(action != null && action.equals("update"))
    	{
    		/*int numsalle = Integer.parseInt(request.getParameter("numsalle"));
    		int numlieu = Integer.parseInt(request.getParameter("numlieu"));
    		double longueur = Double.parseDouble(request.getParameter("longueur"));
    		double largeur = Double.parseDouble(request.getParameter("largeur"));
    		double hauteur = Double.parseDouble(request.getParameter("hauteur"));
    		double surface = Double.parseDouble(request.getParameter("surface"));
    		double tarifdemijsalle = Double.parseDouble(request.getParameter("tarifdemijsalle"));*/
    		
    		this.getServletContext().getRequestDispatcher( "/modifierSalle.jsp" ).forward( request, response );
    	}
    	
        /* Affichage de la page de gestions de salles */
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
       
    }

}
