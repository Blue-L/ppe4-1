<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage>
    <jsp:attribute name="header">
      <h1>Inscription</h1>
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    <h1>Formulaire d'inscription</h1>
   	<form method="post" action="inscription">
   	      		<c:if test = "${not empty form.erreurs['mail']}">
				   	<div class="alert alert-danger" role="alert">
				  		${form.resultat}
				  		<br>
				  		${form.erreurs['mail']}
					</div>
      			</c:if>
      			<c:if test = "${empty form.erreurs}">
	      			<div class="alert alert-success">
	      				${form.resultat}
	      			</div>
      			</c:if>


                <input placeholder="Adresse email" value="" class="form-control" type="email" id="mail" name="mail" size="20" maxlength="60" />
                <br />

                <input placeholder="Mot de passe" value="" class="form-control" type="password" id="mdp" name="mdp" value="" size="20" maxlength="20" />
                <br />

                <input type="submit" value="Inscription" class="btn btn-primary" />
        </form>
    </jsp:body>
</t:genericpage>