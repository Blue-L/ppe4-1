<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage>
	
    <jsp:attribute name="header">
      
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
   		<h1>Gestion des salles</h1>
   		 <table class="table table-striped table-bordered text-center">
   		 <thead>
   		    <tr>
       			<td> Numéro de salle </td>
       			<td> Lieu </td>
       			<td> Longueur </td>
       			<td> Largeur </td>
       			<td> Hauteur </td>
       			<td> Surface </td>
       			<td> Tarif 1/2 journée </td>
       			<td> Actions </td>
       		</tr>
   		 </thead>
		<tbody>
		<c:forEach items="${ salles }" var="salle" varStatus="status">
   		<tr>
   			<td>
				<c:out value="${ salle.numsalle }" /> 
				
			</td>
			<td>
				<c:out value="${ salle.lieu.nomlieu }" /> 
			</td>
			<td>
				<c:out value="${ salle.longueursalle }" /> 
			</td>
			<td>
				<c:out value="${ salle.largeursalle }" /> 
			</td>
			<td>
				<c:out value="${ salle.hauteursalle }" /> 
			</td>
			<td>
				<c:out value="${ salle.surfacesalle }" /> 
			</td>
			<td>
				<c:out value="${ salle.tarifdemijsalle } €" /> 
			</td>
			<td>
				<a class="btn btn-danger" href="salle?action=delete&numsalle=${ salle.numsalle }">Supprimer</a>			
				<a class="btn btn-primary" href="salle?action=update&numsalle=${ salle.numsalle }&numlieu=${ salle.lieu.numlieu }&longueursalle=${ salle.longueursalle }&largeursalle=${ salle.largeursalle }&hauteursalle=${ salle.hauteursalle }&surfacesalle=${ salle.surfacesalle }&tarifdemijsalle=${ salle.tarifdemijsalle }">Modifier</a>			
			</td>
  		 </tr>
	   	</c:forEach>
		</tbody>
       </table>   
       <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
       <script>
       function getUrlVars() {
    	    var vars = {};
    	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    	        vars[key] = value;
    	    });
    	    return vars;
    	}
       
       const action = getUrlVars()['action'];
       
       if (action != null && action == "delete")
   	   {
    	   const numsalle = getUrlVars()['numsalle']
   	   		axios.get('salle?action=delete&numsalle='+numsalle).then(function(response) {
   	   			console.log(response)
   	   			window.location.replace("http://localhost:8080/PPE_4.1/salle");
   	   		})
   	   }
       </script>
    </jsp:body>
</t:genericpage>