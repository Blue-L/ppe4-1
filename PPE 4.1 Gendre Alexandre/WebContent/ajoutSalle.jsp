<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage>
	
    <jsp:attribute name="header">
      
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
   		
   		<div class="container">
   			<h1>Ajout d'une salle</h1>
	   		<form method="post" action="ajoutSalle">
      			<c:if test = "${empty form.erreurs}">
	      			<div class="alert alert-success">
	      				${form.resultat}
	      			</div>
      			</c:if>
		   		<div class="row">
		   		   	<div class="col">
		   				<input type="number" class="form-control" name="longueur" placeholder="Longueur" value="50"/>
		   			</div>
		   			<div class="col">
		   				<input type="number" class="form-control" name="largeur" placeholder="Largeur" value="50"/>
		   			</div>
		   			<div class="col">
		   				<input type="number" class="form-control" name="hauteur" placeholder="Hauteur" value="10"/>
		   			</div>
		   			<div class="col">
		   				<input type="number" class="form-control" name="surface" placeholder="Surface" value="50"/>
		   			</div>
		   		</div>
		   		<div class="row mt-3">
		   			<div class="col">
		   				<input type="number" class="form-control" step="0.01" name="tarif" placeholder="Tarif" value="250" />
					</div>
					<div class="col">
						<select class="form-control" name="lieu">
						<c:forEach items="${ lieus }" var="lieu" varStatus="status">
							<option value="${ lieu.numlieu }"><c:out value="${ lieu.nomlieu }"></c:out></option>
						</c:forEach>
						</select>
					</div>
		   		</div>
		   		
		   		<input type="submit" class="btn btn-primary w-100 mt-3" value="Ajouter" />
	   		</form>
   		</div>
    </jsp:body>
</t:genericpage>