<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/PPE_4.1/index">Meetings Booker</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
         		<li class="nav-item">
        		<a class="nav-link" href="index">Accueil</a>
      		</li>
    	<c:if test="${!empty sessionScope.sessionUtilisateur}">
     		<li class="nav-item">
        		<a class="nav-link" href="deconnexion">Deconnexion</a>
      		</li>
     		<li class="nav-item">
        		<a class="nav-link" href="salle">Salles</a>
      		</li>
      		<li class="nav-item">
        		<a class="nav-link" href="ajoutSalle">Ajout d'une salle</a>
      		</li>
    	</c:if>
    	<c:if test="${empty sessionScope.sessionUtilisateur}">
	      <li class="nav-item">
	        <a class="nav-link" href="inscription">Inscription</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="connexion">Connexion</a>
	      </li>
    	</c:if>
    </ul>
  </div>
</nav>