<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage>
	
    <jsp:attribute name="header">
      <h1>Connexion</h1>
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    <c:if test="${empty sessionScope.sessionUtilisateur}">
    	<h1>Formulaire de connexion</h1>
   			<form method="post" action="connexion">
                <c:if test = "${not empty form.erreurs['mail']}">
				   	<div class="alert alert-danger" role="alert">
				  		${form.resultat}
				  		<br>
				  		${form.erreurs['mail']}
					</div>
      			</c:if>
      			<c:if test = "${empty form.erreurs}">
	      			<div class="alert alert-success">
	      				${form.resultat}
	      			</div>
      			</c:if>
                <input placeholder="Adresse email" value="alex@gmail.com" class="form-control" type="email" id="mail" name="mail" value="<c:out value="${utilisateur.mailutilisateur}"/>" size="20" maxlength="60" />
                <br />

                <input placeholder="Mot de passe" value="agendre" class="form-control" type="password" id="mdp" name="mdp" value="" size="20" maxlength="20" />
                <br />

                <input type="submit" value="Connexion" class="btn btn-primary" />
        	</form>
    </c:if>
    <c:if test="${!empty sessionScope.sessionUtilisateur}">
    	<c:redirect url = "/"/>
    </c:if>
    
    </jsp:body>
</t:genericpage>